package com.soulkey.exercise

import com.soulkey.exercise.data.schedulers.SchedulerProvider

import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler

class TestSchedulerProvider : SchedulerProvider {

    var testScheduler: TestScheduler = TestScheduler()

    override fun computation(): Scheduler {
        return testScheduler
    }

    override fun io(): Scheduler {
        return testScheduler
    }

    override fun ui(): Scheduler {
        return testScheduler
    }

    fun triggerActions() {
        testScheduler.triggerActions()
    }
}
