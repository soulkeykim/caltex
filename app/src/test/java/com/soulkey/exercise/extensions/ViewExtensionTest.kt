package com.soulkey.exercise.extensions

import android.view.View
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class ViewExtensionTest {

    @Mock
    private
    lateinit var mView: View

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testToggleVisibilityVisible() {
        mView.toggleVisibility(true)
        verify(mView).visibility = View.VISIBLE
    }

    @Test
    fun testToggleVisibilityGone() {
        mView.toggleVisibility(false)
        verify(mView).visibility = View.GONE
    }
}