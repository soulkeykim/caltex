package com.soulkey.exercise.extensions

import android.widget.TextView
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.reset
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

class TextViewExtensionTest {

    @Mock
    private
    lateinit var mView: TextView

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testsSetFormatAUD() {
        mView.setFormatAUD(0.0)
        testVerifyText("$0.00")

        mView.setFormatAUD(0.009)
        testVerifyText("$0.01")

        mView.setFormatAUD(10.234)
        testVerifyText("$10.23")

        mView.setFormatAUD(29832.43212)
        testVerifyText("$29,832.43")


        mView.setFormatAUD(192318237.3871239)
        testVerifyText("$192,318,237.39")
    }

    @Test
    fun testsSetFormatDate() {
        mView.setFormatDate(Date(1417352400000))
        testVerifyText("01 DEC 2014")

        mView.setFormatDate(Date(1523500692933))
        testVerifyText("12 APR 2018")

        mView.setFormatDate(Date(1579438800000))
        testVerifyText("20 JAN 2020")
    }

    @Test
    fun testsSetFormatDayAgo() {
        mView.setFormatDayAgo(getDate(1))
        testVerifyText("tomorrow")

        mView.setFormatDayAgo(getDate(2))
        testVerifyText("within 2 days")

        mView.setFormatDayAgo(getDate(-1))
        testVerifyText("yesterday")

        mView.setFormatDayAgo(getDate(-2))
        testVerifyText("2 days ago")

        mView.setFormatDayAgo(getDate(-100))
        testVerifyText("3 months ago")

        mView.setFormatDayAgo(getDate(-1000))
        testVerifyText("2 years ago")
    }

    private fun testVerifyText(expected: String) {
        verify(mView).text = expected
        reset(mView)
    }

    private fun getDate(days: Int): Date {
        return Calendar.getInstance()
                .apply {
                    time = Date()
                    add(Calendar.DATE, days)
                }.time
    }
}