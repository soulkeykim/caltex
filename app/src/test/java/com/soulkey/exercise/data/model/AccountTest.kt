package com.soulkey.exercise.data.model

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class AccountTest {

    private val accountName = "accountName"
    private val accountNumber = "accountNumber"
    private val doubleZero : Double = 0.0
    private val available : Double = 21.4
    private val balance : Double = 32.5

    @Test
    fun testInitWithEmpty() {
        // When
        val account = Account()

        // Then
        assertNull(account.accountName)
        assertNull(account.accountNumber)
        assertEquals(doubleZero, account.available, Double.NaN)
        assertEquals(doubleZero, account.balance, Double.NaN)
    }

    @Test
    fun testInitWithNull() {
        // When
        val account = Account(null, null)

        // Then
        assertNull(account.accountName)
        assertNull(account.accountNumber)
        assertEquals(doubleZero, account.available, Double.NaN)
        assertEquals(doubleZero, account.balance, Double.NaN)
    }

    @Test
    fun testInitWithValues() {
        // When
        val account = Account(accountName, accountNumber, available, balance)

        // Then
        assertEquals(accountName, account.accountName)
        assertEquals(accountNumber, account.accountNumber)
        assertEquals(available, account.available, Double.NaN)
        assertEquals(balance, account.balance, Double.NaN)
    }
}