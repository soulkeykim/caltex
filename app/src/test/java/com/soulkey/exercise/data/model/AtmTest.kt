package com.soulkey.exercise.data.model

import org.junit.Assert.*
import org.junit.Test

class AtmTest {

    private val id = "id"
    private val name = "name"
    private val address = "address"
    private val location = Atm.Location(10.0, 20.0)

    @Test
    fun testInitWithEmpty() {
        // When
        val atm = Atm()

        // Then
        assertNull(atm.id)
        assertNull(atm.name)
        assertNull(atm.address)
        assertNull(atm.location)
    }

    @Test
    fun testInitWithNull() {
        // When
        val atm = Atm(null, null, null, null)

        // Then
        assertNull(atm.id)
        assertNull(atm.name)
        assertNull(atm.address)
        assertNull(atm.location)
    }

    @Test
    fun testInitWithValues() {
        // When
        val atm = Atm(id, name, address, location)

        // Then
        assertEquals(id, atm.id)
        assertEquals(name, atm.name)
        assertEquals(address, atm.address)
        assertEquals(location, atm.location)
        assertEquals(location.lat, atm.location!!.lat, Double.NaN)
        assertEquals(location.lng, atm.location!!.lng, Double.NaN)
    }
}