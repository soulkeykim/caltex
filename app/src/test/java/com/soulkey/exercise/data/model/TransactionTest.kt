package com.soulkey.exercise.data.model

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class TransactionTest {

    val id = "id"
    val effectiveDate = "12/04/2018"
    val description = "description"
    val amount = 23.54
    val atmId = null

    @Test
    fun testInitClearedTransaction() {
        // When
        val transaction = ClearedTransaction(id, effectiveDate, description, amount, atmId)

        // Then
        assertFalse(transaction.isPending)
    }

    @Test
    fun testInitPendingTransaction() {
        // When
        val transaction = PendingTransaction(id, effectiveDate, description, amount, atmId)

        // Then
        assertTrue(transaction.isPending)
    }
}