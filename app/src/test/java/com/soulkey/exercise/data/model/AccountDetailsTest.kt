package com.soulkey.exercise.data.model

import org.junit.Assert.*
import org.junit.Test

class AccountDetailsTest {

    private val account = Account()
    private val clearedTransactions: List<ClearedTransaction> = emptyList()
    private val pendingTransactions: List<PendingTransaction> = emptyList()
    private val atms: List<Atm> = emptyList()

    @Test
    fun testInitWithEmpty() {
        // When
        val accountDetails = AccountDetails()

        // Then
        assertNull(accountDetails.account)
        assertNull(accountDetails.atms)
        assertTrue(accountDetails.getTransactions().isEmpty())
    }

    @Test
    fun testInitWithValues() {
        // When
        val accountDetails = AccountDetails(account, clearedTransactions, pendingTransactions, atms)

        // Then
        assertEquals(account, accountDetails.account)
        assertEquals(clearedTransactions + pendingTransactions, accountDetails.getTransactions())
        assertEquals(atms, accountDetails.atms)
    }
}