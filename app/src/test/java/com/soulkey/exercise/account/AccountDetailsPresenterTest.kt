package com.soulkey.exercise.account

import android.content.Context
import com.soulkey.exercise.TestSchedulerProvider
import com.soulkey.exercise.data.model.Account
import com.soulkey.exercise.data.model.AccountDetails
import com.soulkey.exercise.data.source.AccountDataSource
import com.soulkey.exercise.data.source.AccountRepository
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class AccountDetailsPresenterTest {

    var mSchedulerProvider = TestSchedulerProvider()

    @Mock
    private
    lateinit var mRemoteDataSource: AccountDataSource

    @Mock
    private
    lateinit var mLocalDataSource: AccountDataSource

    @Mock
    private var mView: AccountDetailsContract.View? = null

    @Mock
    lateinit var mContext: Context

    private lateinit var mAccountRepository: AccountRepository
    private val localAccountDetails = AccountDetails(Account("local"))
    private val remoteAccountDetails = AccountDetails(Account("remote"))

    private var presenter: AccountDetailsPresenter? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        mAccountRepository = AccountRepository(mRemoteDataSource, mLocalDataSource)
        presenter = AccountDetailsPresenter(mAccountRepository, mSchedulerProvider)
    }

    @Test
    fun testGetAccountDetails_WhenRemoteAndLocalIsAvailable_ShouldUpdateViews() {
        // When
        `when`(mRemoteDataSource.accountDetails).thenReturn(Single.just(remoteAccountDetails))
        `when`(mLocalDataSource.accountDetails).thenReturn(Single.just(localAccountDetails))

        presenter!!.attachedView(mView!!)
        mSchedulerProvider.triggerActions()

        // Then
        verify(mView)?.toggleProgressView(false)
        verify(mView)?.showAccountDetails(remoteAccountDetails)
    }

    @Test
    fun testGetAccountDetails_WhenRemoteIsUnavailableAndLocalIsAvailable_ShouldUpdateViews() {
        // When
        `when`(mRemoteDataSource.accountDetails).thenReturn(Single.error(Throwable()))
        `when`(mLocalDataSource.accountDetails).thenReturn(Single.just(localAccountDetails))

        presenter!!.attachedView(mView!!)
        mSchedulerProvider.triggerActions()

        // Then
        verify(mView)?.toggleProgressView(false)
        verify(mView)?.showAccountDetails(localAccountDetails)
    }

    @Test
    fun testGetAccountDetails_WhenRemoteAndLocalIsUnavailable_ShouldUpdateViews() {
        // When
        `when`(mRemoteDataSource.accountDetails).thenReturn(Single.error(Throwable()))
        `when`(mLocalDataSource.accountDetails).thenReturn(Single.error(Throwable()))

        presenter!!.attachedView(mView!!)
        mSchedulerProvider.triggerActions()

        // Then
        verify(mView)?.toggleProgressView(false)
        verify(mView)?.showLoadingAccountDetailsError()
    }

    @Test
    fun testGetAccountDetails_WhenDetached_ShouldUpdateViews() {
        // When
        `when`(mRemoteDataSource.accountDetails).thenReturn(Single.just(remoteAccountDetails))
        `when`(mLocalDataSource.accountDetails).thenReturn(Single.just(localAccountDetails))

        presenter!!.attachedView(mView!!)
        presenter!!.detachedView()

        mSchedulerProvider.triggerActions()

        // Then
        verifyZeroInteractions(mView)
    }
}
