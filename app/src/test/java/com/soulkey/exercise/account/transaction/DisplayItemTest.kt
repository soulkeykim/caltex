package com.soulkey.exercise.account.transaction

import com.soulkey.exercise.data.model.Transaction
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock
import java.util.*


class DisplayItemTest {

    @Test
    fun testInitDateInfo() {
        // When
        val dateInfo = DateInfo(mock(Date::class.java))

        // Then
        assertEquals(DisplayItem.TYPE_DATE_INFO, dateInfo.type)
    }

    @Test
    fun testInitTransactionInfo() {
        // When
        val transactionInfo = TransactionInfo(mock(Transaction::class.java))

        // Then
        assertEquals(DisplayItem.TYPE_TRANSACTION_INFO, transactionInfo.type)
    }
}
