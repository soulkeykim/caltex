package com.soulkey.exercise.extensions

import android.view.View

fun View.toggleVisibility(isVisible : Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}