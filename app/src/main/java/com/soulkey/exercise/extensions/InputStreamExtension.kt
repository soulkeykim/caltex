package com.soulkey.exercise.extensions

import java.io.InputStream
import java.nio.charset.Charset

fun InputStream.readTextAndClose(charset: Charset = Charsets.UTF_8): String {
    return bufferedReader(charset).use { it.readText() }
}