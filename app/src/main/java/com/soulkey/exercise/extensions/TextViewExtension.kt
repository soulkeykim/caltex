package com.soulkey.exercise.extensions

import android.widget.TextView
import com.github.marlonlom.utilities.timeago.TimeAgo
import com.soulkey.exercise.extensions.Format.DATE_FORMAT
import java.text.SimpleDateFormat
import java.util.*

internal object Format {
    const val STRING_FORMAT_AUD = "$%,.2f"
    val DATE_FORMAT = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH).apply {
        TimeZone.getTimeZone("Australia/Sydney")
    }
}

fun TextView.setFormatAUD(value: Double) {
    text = String.format(Locale.US, Format.STRING_FORMAT_AUD, value)
}

fun TextView.setFormatDate(date: Date) {
    text = DATE_FORMAT.format(date).toUpperCase()
}

fun TextView.setFormatDayAgo(date: Date) {
    text = TimeAgo.using(date.time)
}
