package com.soulkey.exercise.findus

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.soulkey.exercise.R
import com.soulkey.exercise.data.model.Atm
import kotlinx.android.synthetic.main.activity_maps.*

import timber.log.Timber

class FindUsActivity : AppCompatActivity(), OnMapReadyCallback {

    companion object {
        const val ARGUMENT_ATM = "arg_atm"
        const val DEFAULT_ZOOM_LEVEL = 17

        fun createIntent(context: Context, atm: Atm): Intent {
            return Intent(context, FindUsActivity::class.java)
                    .apply {
                        putExtra(ARGUMENT_ATM, atm)
                    }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        setToolbar()
        setMap()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        setMarker(googleMap)
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun setMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun setMarker(googleMap: GoogleMap?) {
        var atm: Atm? = null
        if (intent.hasExtra(ARGUMENT_ATM)) {
            atm = intent.getSerializableExtra(ARGUMENT_ATM) as Atm
        }

        if (atm != null) {
            val location = atm.location
            val sydney = LatLng(location!!.lat, location.lng)
            googleMap?.apply {
                addMarker(MarkerOptions()
                        .position(sydney)
                        .title(atm.name)
                        .snippet(atm.address)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_foodary)))
                moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, DEFAULT_ZOOM_LEVEL.toFloat()))
            }
        } else {
            finish()
            Timber.e("unexpected situation")
        }
    }
}
