package com.soulkey.exercise.account

import com.soulkey.exercise.di.ActivityScoped

import dagger.Binds
import dagger.Module

@Module
abstract class AccountDetailsModule {

    @ActivityScoped
    @Binds
    abstract fun accountDetailsPresenter(presenter: AccountDetailsPresenter): AccountDetailsContract.Presenter
}