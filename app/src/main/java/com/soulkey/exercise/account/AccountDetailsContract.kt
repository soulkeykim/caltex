package com.soulkey.exercise.account

import com.soulkey.exercise.base.BasePresenter
import com.soulkey.exercise.base.BaseView
import com.soulkey.exercise.data.model.AccountDetails

interface AccountDetailsContract {

    interface View : BaseView {

        fun toggleProgressView(isVisible: Boolean)

        fun showAccountDetails(accountData: AccountDetails)

        fun showLoadingAccountDetailsError()
    }

    interface Presenter : BasePresenter<View>
}
