package com.soulkey.exercise.account.transaction

import com.soulkey.exercise.data.model.Transaction
import java.util.*

abstract class DisplayItem {

    abstract val type: Int

    companion object {
        const val TYPE_DATE_INFO = 0
        const val TYPE_TRANSACTION_INFO = 1
    }
}

class DateInfo internal constructor(val date: Date) : DisplayItem() {
    override val type: Int
        get() = TYPE_DATE_INFO
}

class TransactionInfo internal constructor(val transaction: Transaction) : DisplayItem() {
    override val type: Int
        get() = TYPE_TRANSACTION_INFO
}