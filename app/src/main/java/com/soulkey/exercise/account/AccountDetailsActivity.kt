package com.soulkey.exercise.account

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.soulkey.exercise.R
import com.soulkey.exercise.account.transaction.TransactionAdapter
import com.soulkey.exercise.data.model.AccountDetails
import com.soulkey.exercise.extensions.setFormatAUD
import com.soulkey.exercise.extensions.toggleVisibility
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_account_details.*
import kotlinx.android.synthetic.main.content_account_details.*
import kotlinx.android.synthetic.main.content_account_info.*
import kotlinx.android.synthetic.main.progress_layout.*
import javax.inject.Inject

class AccountDetailsActivity : DaggerAppCompatActivity(), AccountDetailsContract.View {

    @Inject
    lateinit var mPresenter: AccountDetailsPresenter

    private var mTransactionAdapter: TransactionAdapter = TransactionAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_details)

        mPresenter.attachedView(this)

        setSupportActionBar(toolbar)
        supportActionBar!!.setIcon(R.drawable.action_bar_logo)

        setTransactionView()
    }

    override fun onDestroy() {
        mPresenter.detachedView()
        super.onDestroy()
    }

    override fun toggleProgressView(isVisible: Boolean) {
        progress_view.toggleVisibility(isVisible)
    }

    override fun showAccountDetails(accountData: AccountDetails) {
        val account = accountData.account

        // Account
        account_name_tv.text = account!!.accountName
        account_number_tv.text = account.accountNumber
        available_fund_tv.setFormatAUD(account.available)
        account_balance_tv.setFormatAUD(account.balance)

        // Transaction
        mTransactionAdapter.updateAccountDetails(accountData)
    }

    override fun showLoadingAccountDetailsError() {
        Snackbar.make(findViewById(android.R.id.content), getString(R.string.loading_error_message), Snackbar.LENGTH_SHORT).show()
    }

    private fun setTransactionView() {
        transaction_rcv.apply {
            adapter = mTransactionAdapter
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            isNestedScrollingEnabled = false
        }
    }
}
