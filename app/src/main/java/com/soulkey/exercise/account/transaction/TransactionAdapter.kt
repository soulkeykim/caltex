package com.soulkey.exercise.account.transaction

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.soulkey.exercise.R
import com.soulkey.exercise.data.model.AccountDetails
import com.soulkey.exercise.data.model.Atm
import com.soulkey.exercise.data.model.Transaction
import com.soulkey.exercise.extensions.setFormatAUD
import com.soulkey.exercise.extensions.setFormatDate
import com.soulkey.exercise.extensions.setFormatDayAgo
import com.soulkey.exercise.extensions.toggleVisibility
import com.soulkey.exercise.findus.FindUsActivity
import java.util.*

class TransactionAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mDisplayItems = ArrayList<DisplayItem>()
    private var mAtms: List<Atm>? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            DisplayItem.TYPE_DATE_INFO -> DateViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_date_info, viewGroup, false))
            DisplayItem.TYPE_TRANSACTION_INFO -> TransactionViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_transaction_info, viewGroup, false))
            else -> TransactionViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_transaction_info, viewGroup, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder.itemViewType) {
            DisplayItem.TYPE_DATE_INFO -> bindDateInfo(holder, position)
            DisplayItem.TYPE_TRANSACTION_INFO -> bindTransactionInfo(holder, position)
            else -> bindTransactionInfo(holder, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return mDisplayItems[position].type
    }

    override fun getItemCount(): Int {
        return mDisplayItems.size
    }

    fun updateAccountDetails(accountDetails: AccountDetails?) {
        mAtms = accountDetails?.atms

        mDisplayItems.clear()
        mDisplayItems.addAll(getDisplayItems(accountDetails?.getTransactions()))
        notifyDataSetChanged()
    }

    private fun getDisplayItems(transactions: List<Transaction>?): List<DisplayItem> {
        val displayItems = ArrayList<DisplayItem>()
        val groupedMap = transactions
                .orEmpty()
                .groupBy { it.date }
        groupedMap.forEach {
            val date = it.key
            if (date != null) {
                displayItems.add(DateInfo(date))
                it.value.forEach {
                    displayItems.add(TransactionInfo(it))
                }
            }
        }
        return displayItems
    }

    private fun bindDateInfo(holder: RecyclerView.ViewHolder, position: Int) {
        val dateInfo = mDisplayItems[position] as DateInfo
        val dateViewHolder = holder as DateViewHolder
        dateViewHolder.dateTv.setFormatDate(dateInfo.date)
        dateViewHolder.dayAgoTv.setFormatDayAgo(dateInfo.date)
    }

    private fun bindTransactionInfo(holder: RecyclerView.ViewHolder, position: Int) {
        val transactionInfo = mDisplayItems[position] as TransactionInfo
        val transaction = transactionInfo.transaction
        val transactionViewHolder = holder as TransactionViewHolder

        transactionViewHolder.titleTv.text = transaction.refinedDescription
        transactionViewHolder.amountTv.setFormatAUD(transaction.amount)

        val isAtmTransaction = transaction.atmId != null
        transactionViewHolder.atmIv.toggleVisibility(isAtmTransaction)
        if (isAtmTransaction) {
            transactionViewHolder.itemView.setOnClickListener {
                val context = it.context
                for (atm in mAtms!!) {
                    if (atm.id == transaction.atmId) {
                        context.startActivity(FindUsActivity.createIntent(context, atm))
                    }
                }
            }
        } else {
            transactionViewHolder.itemView.setOnClickListener(null)
        }
    }

    internal class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dateTv: TextView = itemView.findViewById(R.id.date_tv)
        var dayAgoTv: TextView = itemView.findViewById(R.id.day_ago_tv)
    }

    internal class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var titleTv: TextView = itemView.findViewById(R.id.title_tv)
        var amountTv: TextView = itemView.findViewById(R.id.amount_tv)
        var atmIv: ImageView = itemView.findViewById(R.id.atm_iv)

    }
}
