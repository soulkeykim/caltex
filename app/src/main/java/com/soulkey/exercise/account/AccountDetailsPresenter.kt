package com.soulkey.exercise.account

import com.soulkey.exercise.data.schedulers.SchedulerProvider
import com.soulkey.exercise.data.source.AccountRepository
import com.soulkey.exercise.di.ActivityScoped
import io.reactivex.disposables.Disposable
import javax.inject.Inject

@ActivityScoped
class AccountDetailsPresenter @Inject
internal constructor(private val mAccountRepository: AccountRepository,
                     private val mSchedulerProvider: SchedulerProvider) : AccountDetailsContract.Presenter {

    private var mView: AccountDetailsContract.View? = null

    private var mAccountDetailsDisposable: Disposable? = null

    override fun attachedView(view: AccountDetailsContract.View) {
        mView = view
        loadAccountData()
    }

    override fun detachedView() {
        mAccountDetailsDisposable!!.dispose()
        mAccountDetailsDisposable = null
        mView = null
    }

    private fun loadAccountData() {
        mAccountDetailsDisposable = mAccountRepository.accountDetails
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        { accountData ->
                            mView!!.toggleProgressView(false)
                            mView!!.showAccountDetails(accountData)
                        },
                        // onError
                        { _ ->
                            mView!!.toggleProgressView(false)
                            mView!!.showLoadingAccountDetailsError() }
                )
    }
}