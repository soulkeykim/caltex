package com.soulkey.exercise.base

interface BasePresenter<in V : BaseView> {

    fun attachedView(view: V)

    fun detachedView()
}