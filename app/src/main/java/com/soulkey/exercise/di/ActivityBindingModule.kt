package com.soulkey.exercise.di

import com.soulkey.exercise.account.AccountDetailsActivity
import com.soulkey.exercise.account.AccountDetailsModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [AccountDetailsModule::class])
    abstract fun accountActivity(): AccountDetailsActivity
}