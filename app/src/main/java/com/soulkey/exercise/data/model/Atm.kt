package com.soulkey.exercise.data.model

import java.io.Serializable

data class Atm(
        val id: String? = null,
        val name: String? = null,
        val address: String? = null,
        val location: Location? = null
) : Serializable {
    data class Location(
            val lat: Double = 0.0,
            val lng: Double = 0.0
    ) : Serializable
}
