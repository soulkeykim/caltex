package com.soulkey.exercise.data.model

import android.text.Html
import android.text.Spanned
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

abstract class Transaction (
        val id: String? = null,
        private val effectiveDate: String? = null,
        val description: String? = null,
        val amount: Double = 0.0,
        val atmId: String? = null
) {

    internal companion object {
        const val PENDING_HTML = "<b>PENDING: </b>"
        const val REMOVABLE_DESC = "<br/>"
        val DATE_FORMAT = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
    }

    val isPending: Boolean
        get() = this is PendingTransaction

    val date: Date?
        get() {
            return try {
                DATE_FORMAT.parse(effectiveDate)
            } catch (e: ParseException) {
                Timber.e(e)
                null
            }
        }

    val refinedDescription: Spanned
        get() {
            return Html.fromHtml(StringBuilder().apply {
                if (isPending) {
                    append(PENDING_HTML)
                }
                append(description!!.replace(REMOVABLE_DESC, " "))
            }.toString())
        }
}

class PendingTransaction(id: String?, effectiveDate: String?, description: String?, amount: Double, atmId: String?) : Transaction(id, effectiveDate, description, amount, atmId)
class ClearedTransaction(id: String?, effectiveDate: String?, description: String?, amount: Double, atmId: String?) : Transaction(id, effectiveDate, description, amount, atmId)