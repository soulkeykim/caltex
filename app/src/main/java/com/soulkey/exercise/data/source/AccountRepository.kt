package com.soulkey.exercise.data.source

import com.soulkey.exercise.data.model.AccountDetails
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AccountRepository @Inject
internal constructor(@Remote private val mAccountRemoteDataSource: AccountDataSource,
                     @Local private val mAccountLocalDataSource: AccountDataSource) : AccountDataSource {

    override val accountDetails: Single<AccountDetails>
        get() = mAccountRemoteDataSource.accountDetails
                .onErrorResumeNext(mAccountLocalDataSource.accountDetails)
}
