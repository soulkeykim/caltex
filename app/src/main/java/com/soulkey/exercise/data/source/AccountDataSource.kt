package com.soulkey.exercise.data.source

import com.soulkey.exercise.data.model.AccountDetails

import io.reactivex.Single

interface AccountDataSource {

    val accountDetails: Single<AccountDetails>
}
