package com.soulkey.exercise.data.model

data class Account (
    val accountName: String? = null,
    val accountNumber: String? = null,
    val available: Double = 0.0,
    val balance: Double = 0.0
)
