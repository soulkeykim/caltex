package com.soulkey.exercise.data.source

import android.content.Context

import com.soulkey.exercise.data.schedulers.AndroidSchedulerProvider
import com.soulkey.exercise.data.source.local.AccountLocalDataSource
import com.soulkey.exercise.data.source.remote.AccountRemoteDataSource
import com.soulkey.exercise.data.service.AccountService
import com.soulkey.exercise.data.schedulers.SchedulerProvider

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class AccountDetailsModule {

    companion object {
        const val BASE_URL = "https://www.dropbox.com/"
    }

    @Singleton
    @Provides
    fun provideSchedulerProvider(): SchedulerProvider {
        return AndroidSchedulerProvider()
    }

    @Singleton
    @Provides
    @Local
    fun provideAccountLocalDataSource(context: Context): AccountDataSource {
        return AccountLocalDataSource(context)
    }

    @Singleton
    @Provides
    @Remote
    fun provideAccountRemoteDataSource(accountService: AccountService): AccountDataSource {
        return AccountRemoteDataSource(accountService)
    }

    @Singleton
    @Provides
    fun provideAccountService(): AccountService {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AccountService::class.java)
    }
}
