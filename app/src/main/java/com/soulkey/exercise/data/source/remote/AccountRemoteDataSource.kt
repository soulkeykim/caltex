package com.soulkey.exercise.data.source.remote

import com.soulkey.exercise.data.source.AccountDataSource
import com.soulkey.exercise.data.model.AccountDetails
import com.soulkey.exercise.data.service.AccountService

import javax.inject.Singleton

import io.reactivex.Single

@Singleton
class AccountRemoteDataSource(private val mAccountService: AccountService) : AccountDataSource {

    override val accountDetails: Single<AccountDetails>
        get() = mAccountService.accountData()
}
