package com.soulkey.exercise.data.model

import com.google.gson.annotations.SerializedName

data class AccountDetails(
        val account: Account? = null,
        @SerializedName("transactions")
        private val clearedTransactions: List<ClearedTransaction> = emptyList(),
        @SerializedName("pending")
        private val pendingTransaction: List<PendingTransaction> = emptyList(),
        val atms: List<Atm>? = null) {

    fun getTransactions(): List<Transaction> {
        return clearedTransactions + pendingTransaction
    }
}
