package com.soulkey.exercise.data.source.local

import android.content.Context
import com.google.gson.GsonBuilder
import com.soulkey.exercise.data.model.AccountDetails
import com.soulkey.exercise.data.source.AccountDataSource
import com.soulkey.exercise.extensions.readTextAndClose
import io.reactivex.Single
import java.io.IOException
import javax.inject.Singleton

@Singleton
class AccountLocalDataSource(private val mContext: Context) : AccountDataSource {

    companion object {
        const val FILE_PATH_DEFAULT_ACCOUNT_DATA = "defaultAccountDetails.json"
    }

    private val defaultAccountDetails: AccountDetails
        get() {
            return try {
                GsonBuilder()
                        .create()
                        .fromJson(
                                mContext.assets
                                        .open(FILE_PATH_DEFAULT_ACCOUNT_DATA)
                                        .readTextAndClose(),
                                AccountDetails::class.java)
            } catch (e: Exception) {
                AccountDetails()
            }
        }

    override val accountDetails: Single<AccountDetails>
        get() = Single.just(defaultAccountDetails)
}
