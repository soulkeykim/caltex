package com.soulkey.exercise.data.service

import com.soulkey.exercise.data.model.AccountDetails

import io.reactivex.Single
import retrofit2.http.GET

interface AccountService {

    companion object {
        const val PATH_ACCOUNT_DATA = "s/tewg9b71x0wrou9/data.json?dl=1"
    }

    @GET(PATH_ACCOUNT_DATA)
    fun accountData(): Single<AccountDetails>
}